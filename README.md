## Koha release tools

The release tools repository contains a collection of scripts to aid release mantainers and release managers in their
tasks during their tenure in their respective roles.

### Installation

Simply clone this repository on your development environment and copy the etc/user.example file to etc/user.yaml 
and update it to reflect your own details.

### Scripts

The most commonly used scripts can be found within the bin/ directory, but they are generally written to be run from 
within your koha source clone repository.

#### bin/koha-push

Used to check for new authors and update bugzilla prior to pushing your branch.

The script should be run from within your koha source directory.

`~/release-tools/bin/koha-push updatebz`
`~/release-tools/bin/koha-push updatebz --range HASH..HEAD`
`~/release-tools/bin/koha-push updatebz --range HASH..HEAD`

Upon execution it will walk through all commits that appear in your local branch 
but do not yet appear upstream. It will prompt you for author details if an 
unrecognised author/email combination is found within a commit and will update 
the tracked etc/dev_map.yaml and etc/sponsor_map.yaml files respectively. You 
should check the resultant diff before commiting and pushing an update to the
release-tools repository. The script will then mark the relevant bugs as 
'Pushed to X', add the relevant version to the versions released box and add 
your configured thankyou comment to the bug.

#### bin/koha-release

Used to build the release notes and report release information.

The script should be called from within your koha source directory.

`~/release-tools/bin/koha-release v19.11.00..HEAD info`
`~/release-tools/bin/koha-release v19.11.00..HEAD notes`
`~/release-tools/bin/koha-release v19.11.00..HEAD notes --silent`
`~/release-tools/bin/koha-release v19.11.00..HEAD html misc/release_notes/release_notes_19_11_01.html`

Upon execution it will walk through all commits in your given range and generate
a set of release notes from their content sourcing data from the commits, 
bugzilla and the translation site.  It will interactively ask you about author 
details should it find a commit with an unrecognised author, unless the --silent
parameter is passed. 

* The `notes` command will output a markdown file.
* The `html` command will read in the aforementioned markdown file and general an html file.

#### roll_release.sh

Used to pack and sign a release tar

#### bin/get_developers.pl

This script will be deprecated soon. It is/was used to maintain the koha copy of 
docs/contributors.yaml and docs/history.txt.

Simply run the script from within your kohaclone and it will update the 
contributors.yaml file and produce a history_new.txt which you can use by 
comparison to update the history.txt.

![Koha Logo](https://wiki.koha-community.org/w/images/KohaILS.png)
